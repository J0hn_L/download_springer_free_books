# **Download_Springer_Free_Books**

Do you want to download all of the boooks in the 'Free+English+tectbooks.xlsx' then this is the project for you

note: _If you want to be specific about which books, just edit it to include only the 
ones you want_

## Requirements
The following packages need to be installed, *I've been told its highly recomended you create a new virtual enviroment, but hey, live life on the edge if you want*:

If using Anaconda, run the following from the directroy where the respoitory is downloaded to.
Feel free to call it what you want...

` conda create --name shiny_new_env python=3.7`  
`conda activate py35`  
`pip install -r requirements.txt`  

then open your favorite ide or jupiter note book, run the code and enjoy your new books :)


side note:
those not using conda, create a vertual enviroment and run the follwoing. 
`$ pip install -r requirements.txt`

otherwise you can pip install them packages one by one... 

`pip install pandas`  
`pip install wget`  
`pip install requests`  
`pip install xlrd`  
