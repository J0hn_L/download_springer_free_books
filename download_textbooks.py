import requests
import wget
import pandas as pd
import glob
import os

# create download folder
if not os.path.exists('./download/'):
    os.mkdir('./download/')

# read in the book names
df = pd.read_excel("Free+English+textbooks.xlsx")
print(df.head(10))
files = glob.glob('./download/*')
name = []
for i in files:
    name.append(i.split('\\')[1].split('_')[0])

df = df[~df['Book Title'].isin(name)].reset_index()
print(f'\n \n there are {len(df)} book to download... \n \n')

# watch them go like hot cakes.
for index, row in df.iterrows():
    file_name = f"{row.loc['Book Title'].replace('/', ' or ').replace(':', '')}_{row.loc['Edition']}"
    url = f"{row.loc['OpenURL']}"
    r = requests.get(url)
    download_url = f"{r.url.replace('book','content/pdf')}.pdf"
    book = wget.download(download_url, fr"./download/{file_name}.pdf")
    print(f"----- {np.round(100*(index+1)/len(df),2)}% ----- downloading {row.loc['Book Title']}_{row.loc['Edition']}.pdf Complete ....")
